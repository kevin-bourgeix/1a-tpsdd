#ifndef ARBRE__H
#define ARBRE__H

/**
 * Arbre classique en représentation LVLH
 * val : valeure contenue dans l'arbre
 * lv : Lien vertical vers une autre cellule
 * lh : Lien horizontal vers une autre cellule
 */
typedef struct arbre
{
	char val;
	struct arbre * lv;
	struct arbre * lh;
} arbre_t;

/**
 * Arbre doublement chainé en représentation LVLH
 * Chaque cellule peut remonter vers son père.
 * Le/Les racine(s) de l'arbre ont un père qui a pour valeur NULL.
 */
typedef struct arbrePere
{
	char val;
	struct arbrePere * lv;
	struct arbrePere * lh;
	struct arbrePere * pere;
} arbrePere_t;


/** Prototypes **/
arbre_t * allouerCellules(int nombre);
arbre_t * creationArbrePrefix(char * nomFichier, int * ERRCODE);
void afficherNotationPostfix(arbre_t *a);
arbrePere_t * allouerCelluleArbrePere();
arbrePere_t * transformerArbre(arbre_t * a);
void afficherParcoursPostfixPere(arbrePere_t * a);
void libererArbre(arbre_t * a);
void libererArbrePere(arbrePere_t * a);
arbre_t *recherchePere(arbre_t *a, char p);
arbre_t **parcoursFilstrie(arbre_t *pere,char v);
void insertion(arbre_t *a,char p,char f);


#endif
