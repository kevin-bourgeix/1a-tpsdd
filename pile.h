#ifndef PILE_H
#define PILE_H

#include "arbre.h"

/* On choisit le type ou la strucure empilée ou enfilée en changeant la ligne ci-dessous */
typedef arbre_t * typeP;

typedef struct pile
{
	int taille;
	int sommet;
	typeP *deb;
}pile;


pile *initialisation(int t);

int empiler(pile *tete, typeP v);

typeP sommet(pile *tete);

typeP depiler(pile *tete);

int estVidePile(pile *tete);

void afficher_pile (pile *tete);
void libererPile(pile *tete);

#endif
