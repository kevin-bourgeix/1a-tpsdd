#include <stdio.h>
#include <stdlib.h>

#include "file.h"




file *init(int t)
{
    file *f;
    typeF *nouv;

    f=(file*)malloc(sizeof(file));

    if(f!=NULL)
    {
        nouv=(typeF*)malloc(sizeof(typeF)*t);
        f->fil=&(*nouv);

        if(nouv!=NULL)
        {
            f->taille=t;
            f->nb=0;
            f->fin=0 ;
            f->deb=0;
        }
        else
        {
            free(f);
            f=NULL;
        }
    }
    return f;


}


int enfiler(file *f, typeF v)
{
    int res=0;

    if(f->nb==f->taille)
    {
        printf("La file est pleine\n");
    }
    else
    {
        if(f->fin>=f->taille)
        {
            f->fin=0;
        }

        f->nb=f->nb+1;
        *(f->fil + f->fin)=v;
        f->fin=f->fin +1;
        res=1;
    }
    return res;
}



typeF defiler(file*f)
{
    typeF v = NULL;

    if(f->nb!=0)
    {
        v = *(f->fil + f->deb);
        f->deb=f->deb +1;
        f->nb= f->nb -1;

        if(f->deb>=f->taille)
        {
            f->deb=0;
        }

    }

    return v;
}


int estVideFile(file *f)
{
    return f->nb==0;
}

/**
 * Fonctionne uniquement si TypeF est un short/int/long
 */
void afficher_file(file*f)
{
    int i;
    typeF *cour=f->fil;

    printf("\n");
    printf("File-> ");
    printf("|%d|",f->taille);
    printf("%d|",f->nb);
    printf("%d|",f->fin);
    printf("%d|",f->deb);
    printf(" |->|");

    for(i=0;i < f->taille ; i++)
    {
        printf("%d|", *cour);
        cour = cour+1;
    }
    printf("\n\n");
}

void liberer_file(file * f)
{
    free(f->fil);
    free(f);
    f = NULL;
}

