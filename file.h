#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include "arbre.h"
/* On choisit le type ou la strucure empiler ou enfiler en changeant la ligne ci-dessous */
typedef arbre_t * typeF;

typedef struct file
{
    int taille;
    int nb;
    int fin;
    int deb;
    typeF *fil;
}file;


file *init(int t);

int enfiler(file *f, typeF v);

typeF defiler(file*f);

int estVideFile(file *f);

void afficher_file(file*f);

void liberer_file(file * f);

#endif /* FILE_H_INCLUDED */
