#include <stdio.h>
#include <stdlib.h>
#include "pile.h"
#include "file.h"

#define TAILLE 42

/**
 * Fonction allouerCellules
 * Alloue n cellules d'arbre à l'adresse indiquée
 *
 * INPUT :
 * 		Nombre : nombre de cellules à allouer
 *
 * OUTPUT :
 * 		Adresse de la première cellule.
 *
 */
arbre_t * allouerCellules(int nombre)
{
	arbre_t * ad = NULL;
	arbre_t *cour = NULL;
	int i = 0;
	int memoryBug = 0;

	while(i<nombre && !memoryBug)
	{
		if(i==0)
		{
			ad = (arbre_t *) malloc(sizeof(arbre_t));
			if(ad == NULL) memoryBug = 1;
			else
			{
				cour = ad;
				cour->lh = NULL;
				cour->lv = NULL;
			}
		}
		else
		{
			cour->lh = (arbre_t *) malloc(sizeof(arbre_t));
			if(cour->lh == NULL) memoryBug = 1;
			else cour = cour->lh;
		}
		cour->lv = NULL;
		i++;
	}

	if(cour != NULL) cour->lh = NULL; /* Nécessaire sinon la liste des frères ne fini pas par NULL et ça plante */

	if(memoryBug)
	{
		libererArbre(ad);
		ad = NULL;
	}
	return ad;
}



/**
 * Fonction allouerCelluleArbrePere
 * Alloue une cellule d'arbrePere à l'adresse indiquée
 *
 * OUTPUT : Adresse de la nouvelle cellule
 */
arbrePere_t * allouerCelluleArbrePere()
{
	arbrePere_t * nouv = (arbrePere_t *) malloc(sizeof(arbrePere_t));
	if(nouv != NULL)
	{
		nouv->lv = NULL;
		nouv->lh = NULL;
		nouv->pere = NULL;
	}

	return nouv;
}


/**
 * Fonction creationArbrePrefix
 * Crée un arbre à partir d'une notation préfixée.
 * Hyp : La notation préfixée est supposée valide.
 *
 * INPUT :
 * 		nomFichier : Nom du fichier à ouvrir.
 * 		ERRCODE : Code d'erreur. Retourne 1 si le fichier n'est pas trouvé
 *
 * OUTPUT :
 * 		Adresse du nouvel arbre
 *
 */
arbre_t * creationArbrePrefix(char * nomFichier, int * ERRCODE)
{
	pile * ad_pile = initialisation(TAILLE);
	arbre_t * t = NULL;
	arbre_t **prec = NULL;
	arbre_t *cour = NULL;
	int nbTemp;
	char chTemp;
	FILE *fichier = fopen(nomFichier,"r");
	if(fichier != NULL)
	{
		*ERRCODE = 0;
		fscanf(fichier,"%d",&nbTemp);
		t = allouerCellules(nbTemp);
		prec = &t;
		cour = t;
		empiler(ad_pile,cour);
		
		while(cour != NULL && !estVidePile(ad_pile))
		{
			fscanf(fichier,"%c%d",&chTemp,&nbTemp);
			
			cour->val = chTemp;
			if(nbTemp == 0)
			{
				cour = cour->lh;
				while(cour == NULL && !estVidePile(ad_pile))
				{
						cour = depiler(ad_pile);
						cour = cour->lh;
				}
			}
			else
			{
				prec = &(cour->lv);
				empiler(ad_pile,cour);
				*prec = allouerCellules(nbTemp);
				cour = *prec;
			}
		}
		libererPile(ad_pile);
		
		fclose(fichier);
	}
	else
	{
		*ERRCODE = 1;
	}
	
	return t;
}



/**
 * Fonction afficherNotationPostfix
 * Affiche le parcours postfix d'un arbre
 *
 * INPUT :
 * 		a : Arbre à afficher
 *
 * OUTPUT :
 * 		Copie dans la sortie standard le parcours postfix
 *
 * PROBLEME :
 * 		Dans un premier temps, nous avons fait la notation postfixée. Le problème est que nous n'avons pas implémenté
 * 		une pile prenant le nombre de fils ainsi que l'adresse. Le faire à ce point du TP nous demanderai de refaire
 * 		toutes les fonctions... Pour régler cela, on crée un tableau d'entiers pour simuler une pile d'entier. L'accès
 * 		est rapide puisqu'il s'agit juste d'incrémentations et d'affichages. Cette modification a été approuvée par
 * 		l'enseignant.
 */
void afficherNotationPostfix(arbre_t *a)
{
	arbre_t * cour = a;
	int fin = 0;
	int * fils_pile = (int *) calloc(TAILLE,sizeof(int));
	pile * ad_pile = initialisation(TAILLE);

	while(!fin)
	{
		while(cour != NULL)
		{
			empiler(ad_pile,cour);
			fils_pile++; /* Empiler Entier */
			cour = cour->lv;
		}
		if(!estVidePile(ad_pile))
		{
			cour = depiler(ad_pile);
			printf("(%c,%d)",cour->val,*fils_pile);
			cour = cour->lh;
			*fils_pile = 0; /* Dépilage entier. On le fait après l'affichage, sinon les deux piles ne sont pas synchrones */
			fils_pile--;
			(*fils_pile)++;

		}
		else
		{
			fin = 1;
		}
	}
	libererPile(ad_pile);
	free(fils_pile); /* Libération pile d'entiers */
}

/**
 * Fonction transformerArbre
 * Transforme un arbre en arbrePere
 *
 * INPUT :
 * 		a : arbre à transformer
 *
 * OUTPUT :
 * 		Adresse de l'arbrePere résidu de la transformation
 *
 */
arbrePere_t * transformerArbre(arbre_t * a)
{
	pile * ad_pile = initialisation(TAILLE);
	arbre_t * cour = a;
	arbrePere_t * arbrePere = NULL;
	if(cour != NULL)
	{
		arbrePere = allouerCelluleArbrePere(); 
		if(arbrePere != NULL)
		{
			arbrePere_t * precPere = NULL;
			arbrePere_t * courPere = arbrePere;
			int fin = 0;

			while(!fin)
			{
				while(cour != NULL)
				{
					courPere->val = cour->val;
					courPere->pere = precPere;
					if(cour->lv != NULL)
					{
						courPere->lv = allouerCelluleArbrePere();
					}
					empiler(ad_pile,cour);
					cour = cour->lv;
					precPere = courPere;
					courPere = courPere->lv;
				}
				if(!estVidePile(ad_pile))
				{
					cour = depiler(ad_pile);
					cour = cour->lh;
					courPere = precPere;
					precPere = precPere->pere;
					if(cour != NULL)
					{
						courPere->lh = allouerCelluleArbrePere();
					}
					courPere = courPere->lh;
				}
				else
				{
					fin = 1;
				}
			}
		}
	}

	libererPile(ad_pile);
	return arbrePere;
}



/**
 * Fonction afficherParcoursPostfixPere
 * Affiche le parcours postfixe de l'arbrePere
 *
 * Avantages comparé à l'algorithme normal :
 * 		Ici la pile est remplacée par un entier qui compte à quel point on est descendu dans l'arbre.
 * 		On utilise un pointeur prec pour remonter dans l'arbre
 * INPUT :
 * 		a : Adresse de l'arbrePere
 * OUTPUT :
 * 		Affichage dans la sortie standard du parcours postfixe.
 */
void afficherParcoursPostfixPere(arbrePere_t * a)
{
	arbrePere_t *prec = NULL; /* Remplace virtuellement le dépilage, permet de remonter dans l'arbre */
    arbrePere_t * cour = a;
    int fin = 0;
	int flag = 0; /* Remplace virtuellement la pile (permet de voir si la pile est vide ou non) */

    while(!fin)
    {
        while(cour != NULL)
        {
			prec = cour;
            cour = cour->lv;
			flag++;
        }
		if(flag != 0)
		{
			cour = prec;
			prec = prec->pere;
			if(cour != NULL) printf("%c",cour->val);
			fflush(stdout);
			cour = cour->lh;
			flag--;
		}
		else
		{
			fin = 1;
		}
    }
}

/**
 * Fonction libererArbre
 * Libère un arbre
 *
 * INPUT :
 * 		a : Arbre à libérer
 *
 * OUTPUT :
 * 		Rien
 *
 *
 */
void libererArbre(arbre_t * a)
{
	pile * ad_pile = initialisation(TAILLE);
	arbre_t * cour = a;
	arbre_t * tmp = NULL;
	int fin = 0;

	while(!fin)
	{
		while(cour != NULL)
		{
			empiler(ad_pile,cour);
			cour = cour->lv;
		}
		if(!estVidePile(ad_pile))
		{
			tmp = depiler(ad_pile);
			cour = tmp->lh;
			tmp->lh = NULL;
			tmp->lv = NULL;
			free(tmp);
		}
		else
		{
			fin = 1;
		}
	}
	libererPile(ad_pile);
}


/**
 * Fonction libererArbrePere
 * Libère la mémoire d'un arbrePere
 *
 * INPUT :
 * 		a : Arbre à libérer
 *
 * OUTPUT :
 * 		Rien
 */
void libererArbrePere(arbrePere_t * a)
{
	arbrePere_t * prec = NULL;
	arbrePere_t * cour = a;
	arbrePere_t * tmp = NULL;
	int fin = 0;
	int flag = 0;

	while(!fin)
	{
		while(cour != NULL)
		{
			prec = cour;
			cour = cour->lv;
			flag++;
		}
		if(flag != 0)
		{
			cour = prec;
			prec = prec->pere;
			tmp = cour;
			cour = tmp->lh;
			tmp->lh = NULL;
			tmp->lv = NULL;
			free(tmp);
			flag--;
		}
		else
		{
			fin = 1;
		}
	}
}

/**
 * Fonction recherchePere
 * 
 * Recherche l'adresse du père avec le 1er ordre par niveau.
 * 
 * INPUT :
 * 		a : Arbre courant
 * 		p : Valeure du père
 * 
 * OUTPUT :
 * 		cour : Adresse du père
 */
arbre_t *recherchePere(arbre_t *a, char p)
{
	arbre_t *cour=a; /*Initialisation des pointeurs*/
	int fin=0;
	file *f = init(TAILLE);
	

	while(!fin)
	{
		while((cour!=NULL) && (cour->val!=p))
		{
			enfiler(f,cour);
			cour=cour->lh;
		}
		if(cour != NULL && cour->val == p)
		{
			fin = 1;
		}
		else if(!estVideFile(f))
		{
			cour=defiler(f);
			cour=cour->lv;
		}
		else
		{
			fin=1;
		}

	}

	liberer_file(f);
	return cour;


}

/**
 * Fonction parcoursFilstrie
 * 
 * Recherche dans les fils où placer le point v selon l'ordre lexicographique.
 *
 * INPUT :
 * 		pere : Adresse de la cellule pere
 * 		v : valeur du nouveau fils
 */
arbre_t **parcoursFilstrie(arbre_t *pere,char v)
{
	arbre_t **prec = &(pere->lv);
	arbre_t *cour=pere->lv;

	while((cour!=NULL) && (cour->val<v))
	{
		prec = &(cour->lh);
		cour=cour->lh;
	}

	return prec;
}

/**
 * Fonction insertion
 * 
 * Fonction d'insertion d'une nouvelle lettre.
 * Insère le noeud de valeur f ayant pour père le noeud de valeur p.
 *
 * INPUT :
 * 		a : Arbre où on insert
 * 		p : Père
 * 		f : Fils
 * 
 */
void insertion(arbre_t *a,char p,char f)
{
	arbre_t *pere = NULL;
	arbre_t **finFils = NULL;
	arbre_t *nouv = NULL;

	pere = recherchePere(a,p);

	if(pere!=NULL)
	{
		finFils=parcoursFilstrie(pere,f);
		nouv=allouerCellules(1);
		nouv->val = f;

		if(nouv!=NULL)
		{
			if(*finFils != NULL)
			{
				nouv->lh = *finFils;
				*finFils=nouv;
			}
			else
			{
				pere->lv = nouv;
			}
		}
	}
}
