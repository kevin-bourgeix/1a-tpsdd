#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arbre.h"

/**
 * fonction help
 *
 * Affiche l'aide contextuelle
 */
void help()
{
	printf("Aide\n"
				   "Entrez le nom du fichier en argument\n"
				   "Options a placer apres le nom du fichier :\n"
				   "\tx y : Insere la valeur y avec pour pere x\n");
}

int main(int argc, char ** argv)
{
	char pere;
	char fils;
	arbre_t * arbre = NULL;
	arbrePere_t * arbrePere = NULL;
	int ERRCODE;

	if(argc == 1)
	{
		help();
	}
	else
	{
		arbre = creationArbrePrefix(argv[1],&ERRCODE);
		if(!ERRCODE)
		{
			if(argc == 4)
			{
				pere = argv[2][0];
				fils = argv[3][0];

				insertion(arbre,pere,fils);
			}
			arbrePere = transformerArbre(arbre);
			printf("Notation Postfixee :\n");
			afficherNotationPostfix(arbre);
			printf("\nParcours Postfixee de l'arbrePere :\n");
			afficherParcoursPostfixPere(arbrePere);
			printf("\n");
		}
		else
		{
			printf("Fichier Non trouve\n\n");
		}

	}
	if(arbre != NULL) libererArbre(arbre);
	if(arbrePere != NULL) libererArbrePere(arbrePere);
	
	return 0;
}


