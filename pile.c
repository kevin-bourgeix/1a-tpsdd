#include<stdio.h>
#include<stdlib.h>
#include "pile.h"

pile *initialisation(int t)
{
	pile *tete;
	typeP *nouv;

	tete=(pile*)malloc(sizeof(pile));


	if(tete!=NULL)
	{
		nouv=(typeP*)malloc(sizeof(typeP)*t);
		tete->deb=&(*nouv);


		if(nouv!=NULL)
		{
			tete->taille=t;
			tete->sommet=-1;
		}
		else
		{
			free(tete);
			tete = NULL;
		}
	}

		return tete;
}

int empiler(pile *tete,typeP v)
{

	int res=0;
	if(tete->sommet < tete->taille-1)
	{
		tete->sommet=tete->sommet+1;
		*(tete->deb + tete->sommet)=v;
		res=1;
	}

	return res;
}


typeP sommet(pile *tete)
{
	typeP val;
	val=*(tete->deb + tete->sommet);

	return val;
}



typeP depiler(pile *tete)
{
	typeP val;
	if(tete!=NULL && !estVidePile(tete))
    {
        val = *(tete->deb + tete->sommet);
        tete->sommet= tete->sommet-1;

    }

	return val;
}

int estVidePile(pile *tete)
{
    return tete->sommet==-1;
}

void libererPile(pile *tete)
{
	free(tete->deb);
	free(tete);
	tete=NULL;
}

/*
void afficher_pile (pile *tete)
{
	int i;
	typeP *cour = tete->deb;


	printf("\n");
	printf("Pile-> ");
	printf("|%d|", tete->taille);
	printf("%d|", tete->sommet);
	printf(" |->|");

	for(i=0;i < tete->taille ; i++)
	{
		printf("%d|", *cour);
		cour = cour+1;
	}
	printf("\n\n");
}

*/
