#compilateur
CC = gcc
#options
CFLAGS= -Wall -ansi -pedantic -Wextra -g
LDFLAGS = -lm

#Fichiers objets
OBJ = main.o arbre.o pile.o file.o

#Production Finale
arbre : $(OBJ)
		$(CC) $(OBJ) $(LDFLAGS) -o arbre

#Production par fichier
arbre.o : arbre.h arbre.c
		 $(CC) -c arbre.c $(CFLAGS)

pile.o : pile.h pile.c
		$(CC) -c pile.c $(CFLAGS)

file.o : file.h file.c
		$(CC) -c file.c $(CFLAGS)

main.o : main.c arbre.h
		$(CC) -c main.c $(CFLAGS)
